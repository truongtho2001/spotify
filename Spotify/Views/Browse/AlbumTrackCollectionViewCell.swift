//
//  AlbumTrackCollectionViewCell.swift
//  Spotify
//
//  Created by rta on 1/9/24.
//

import Foundation
import UIKit

class AlbumTrackCollectionViewCell: UICollectionViewCell {
	static let indentifier = "AlbumTrackCollectionViewCell"
	
	private let albumCoverImageView: UIImageView = {
		let imageView = UIImageView()
		imageView.image = UIImage(systemName: "photo")
		imageView.contentMode = .scaleAspectFill
		return imageView
	} ()
	
	private let trackNameLabel: UILabel = {
		let label = UILabel()
		label.numberOfLines = 0
		label.font = .systemFont(ofSize: 18, weight: .regular)
		return label
	}()
	
	private let artistNameLabel: UILabel = {
		let label = UILabel()
		label.font = .systemFont(ofSize: 15, weight: .thin)
		label.numberOfLines = 0
		return label
	}()
	override init(frame: CGRect) {
		super.init(frame: frame)
		backgroundColor = .secondarySystemBackground
		contentView.backgroundColor = .secondarySystemBackground
		contentView.addSubview(trackNameLabel)
		contentView.addSubview(artistNameLabel)
		contentView.clipsToBounds = true
	}
	
	required init?(coder: NSCoder) {
		fatalError()
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		trackNameLabel.frame = CGRect(
			x: 10,
			y: 0,
			width: contentView.width-15,
			height: contentView.height/2
		)
		artistNameLabel.frame = CGRect(
			x: 10,
			y: contentView.height/2,
			width: contentView.width-15,
			height: contentView.height/2
		)
	}
	
	override func prepareForReuse() {
		super.prepareForReuse()
		trackNameLabel.text = nil
		artistNameLabel.text = nil
	}
	
	func configure(with viewModel: RecommendedTrackCellViewModel) {
		trackNameLabel.text = viewModel.name
		artistNameLabel.text = viewModel.artistName
	}
}
