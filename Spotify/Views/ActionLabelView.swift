//
//  ActionLabelView.swift
//  Spotify
//
//  Created by rta on 2/5/24.
//

import UIKit

struct ActionLabelViewModel {
	let text: String
	let acttionTitle: String
}

protocol ActionLabelViewDelegate: AnyObject {
	func actionLabelViewDidTapButton(_ actionView: ActionLabelView)
}
class ActionLabelView: UIView {
	
	weak var delegate: ActionLabelViewDelegate?
	
	private let label: UILabel = {
		let label = UILabel()
		label.textAlignment = .center
		label.numberOfLines = 0
		label.textColor = .secondaryLabel
		return label
	}()
	
	private let button: UIButton = {
		let button = UIButton()
		button.setTitleColor(.link, for: .normal)
		return button
	}()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		//isHidden = true
		addSubview(label)
		addSubview(button)
		button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
	}

	required init?(coder: NSCoder) {
		fatalError()
	}
	
	@objc func didTapButton() {
		delegate?.actionLabelViewDidTapButton(self)
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		button.frame = CGRect(x: 0, y: height-40, width: width, height: 40)
		label.frame = CGRect(x: 0, y: 0, width: width, height: height-45)
		
	}
	 
	func configture(with viewModel: ActionLabelViewModel) {
		label.text = viewModel.text
		button.setTitle(viewModel.acttionTitle, for: .normal)
	}
}
