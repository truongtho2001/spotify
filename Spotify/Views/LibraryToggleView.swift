//
//  LibraryToggleiew.swift
//  Spotify
//
//  Created by rta on 2/4/24.
//

import UIKit

protocol LibraryToggleViewDelegate: AnyObject {
	func libraryToggleViewDidTapPlayLists(_ toggleView: LibraryToggleView)
	func libraryToggleViewDidTappAlbums(_ toggleView: LibraryToggleView)
}

class LibraryToggleView: UIView {
	
	enum State {
		case playlist
		case album
	}
	
	var state: State = .playlist
	weak var delegate: LibraryToggleViewDelegate?
	private let playlistsButton: UIButton = {
		let button = UIButton()
		button.setTitleColor(.label, for: .normal)
		button.setTitle("PlayLists", for: .normal)
		return button
	}()
	
	private let albumsButton: UIButton = {
		let button = UIButton()
		button.setTitleColor(.label, for: .normal)
		button.setTitle("Albums", for: .normal)
		return button
	}()
	
	private let indicatorView: UIView = {
		let view = UIView()
		view.backgroundColor = .systemGreen
		view.layer.masksToBounds = true
		view.layer.cornerRadius = 4
		return view
	}()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		addSubview(playlistsButton)
		addSubview(albumsButton)
		addSubview(indicatorView)
		playlistsButton.addTarget(self, action: #selector(didTapPlayLists), for: .touchUpInside)
		albumsButton.addTarget(self, action: #selector(didTapAlbum), for: .touchUpInside)
	}
	
	@objc private func didTapPlayLists() {
		state = .playlist
		UIView.animate(withDuration: 0.2) {
			self.layoutIndicator()
		}
		delegate?.libraryToggleViewDidTapPlayLists(self)
	}
	
	@objc private func didTapAlbum() {
		state = .album
		UIView.animate(withDuration: 0.2) {
			self.layoutIndicator()
		}
		delegate?.libraryToggleViewDidTappAlbums(self)
	}
	
	required init?(coder: NSCoder) {
		fatalError()
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		playlistsButton.frame = CGRect(x: 0, y: 0, width: 100, height: 50)
		albumsButton.frame = CGRect(x: playlistsButton.right, y: 0, width: 100, height: 50)
		layoutIndicator()
	}
	
	func layoutIndicator() {
		switch state {
			case .playlist:
				indicatorView.frame = CGRect(
					x: 0,
					y: playlistsButton.bottom,
					width: 100, height: 3
				)
			case .album:
				indicatorView.frame = CGRect(
					x: 100,
					y: playlistsButton.bottom,
					width: 100, height: 3
				)
		}
	}
	
	func update(for state: State) {
		self.state = state
		UIView.animate(withDuration: 0.2) {
			self.layoutIndicator()
		}
	}
}
