//
//  PlaylistHeaderCollectionReusableView.swift
//  Spotify
//
//  Created by rta on 1/7/24.
//

import UIKit
import SDWebImage

protocol PlaylistHeaderCollectionReusableViewDelegate: AnyObject {
	func playlistHeaderCollectionReusableViewDidTapPlayAll(_ header: PlaylistHeaderCollectionReusableView)
}

final class PlaylistHeaderCollectionReusableView: UICollectionReusableView {
	static let identifier = "PlaylistHeaderCollectionReusableView"
	
	weak var delegate: PlaylistHeaderCollectionReusableViewDelegate?
	
	private let  nameLabel: UILabel  = {
		let label = UILabel()
		label.font = .systemFont(ofSize: 22,weight: .semibold)
		return label
	}()
	
	private let  descriptionLabel: UILabel  = {
		let label = UILabel()
		label.textColor = .secondaryLabel
		label.font = .systemFont(ofSize: 22,weight: .regular)
		label.numberOfLines = 0
		return label
	}()
	
	private let  owerLabel: UILabel  = {
		let label = UILabel()
		label.textColor = .secondaryLabel
		label.font = .systemFont(ofSize: 22,weight: .light)
		return label
	}()
	
	private let imageView: UIImageView = {
		let imageView = UIImageView()
		imageView.contentMode  = .scaleAspectFit
		imageView.image =  UIImage(named: "photo")
		return imageView
	}()
	
	private let playAllButton: UIButton = {
		let button = UIButton()
		button.backgroundColor = .systemGreen
		let image = UIImage(systemName:"play.fill",withConfiguration: UIImage.SymbolConfiguration(pointSize: 30, weight: .regular))
		button.setImage(image,for: .normal)
		button.tintColor = .white
		button.layer.cornerRadius = 30
		button.layer.masksToBounds = true
		return button
	}()
	
	// MARK: - Iint
	override init(frame: CGRect) {
		super.init(frame: frame)
		backgroundColor  = .systemBackground
		addSubview(nameLabel)
		addSubview(descriptionLabel)
		addSubview(owerLabel)
		addSubview(imageView)
		addSubview(playAllButton)
		playAllButton.addTarget(self, action: #selector(didTapPlayAll), for: .touchUpInside)
		
	}
	
	required init?(coder: NSCoder) {
		fatalError()
	}
	
	@objc private func didTapPlayAll() {
		delegate?.playlistHeaderCollectionReusableViewDidTapPlayAll(self)
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		let imageSize: CGFloat = height/1.8
		imageView.frame = CGRect(x: (width - imageSize)/2, y: 20, width: imageSize, height: imageSize)
		nameLabel.frame = CGRect(x: 10, y: imageView.bottom, width: width-20, height: 44)
		descriptionLabel.frame = CGRect(x: 10, y: nameLabel.bottom, width: width-20, height: 44)
		owerLabel.frame = CGRect(x: 10, y: descriptionLabel.bottom, width: width-20, height: 44)
		playAllButton.frame = CGRect(x: width-100, y: height-70, width: 60, height: 60)
		
	}
	
	func configure(with viewModel: PlaylistHeaderViewModel) {
		nameLabel.text = viewModel.name
		owerLabel.text = viewModel.owerName
		descriptionLabel.text = viewModel.description
		imageView.sd_setImage(with: viewModel.artworkURL,placeholderImage: UIImage(systemName: "photo"),completed: nil)
	}
}
