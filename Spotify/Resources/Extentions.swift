//
//  Extentions.swift
//  Spotify
//
//  Created by rta on 12/10/23.
//

import Foundation
import UIKit

extension UIView {
    var width : CGFloat {
        return frame.size.width
    }
    
    var height : CGFloat {
        return frame.size.height
    }
    
    var left : CGFloat {
        return frame.origin.x
    }
    
    var right : CGFloat {
        return left + width
    }
    
    var top : CGFloat {
        return frame.origin.y
    }
    
    var bottom : CGFloat {
        return top + height
    }
}

extension DateFormatter {
	static let dateFormatter: DateFormatter = {
		let dateFormater = DateFormatter()
		dateFormater.dateFormat = "YYYY-MM-dd"
		return dateFormater
	}()
	
	static let displayFormatter: DateFormatter = {
		let dateFormater = DateFormatter()
		dateFormater.dateStyle = .medium
		return dateFormater
	}()
}

extension String {
	static func formattedDate(string: String) -> String {
		guard let  date = DateFormatter.dateFormatter.date(from: string) else {
			return string
		}
		return DateFormatter.displayFormatter.string(from: date)
	}
}

extension Notification.Name {
	static let albumSavedNotification = Notification.Name("albumSavedNotification")
}
