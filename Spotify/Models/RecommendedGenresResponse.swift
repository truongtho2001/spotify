//
//  RecommendedGenresResponse.swift
//  Spotify
//
//  Created by rta on 12/28/23.
//

import Foundation

struct RecommendedGenresResponse: Codable {
    let genres: [String]
}
