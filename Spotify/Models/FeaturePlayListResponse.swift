//
//  FeaturePlayListResponse.swift
//  Spotify
//
//  Created by rta on 12/28/23.
//
import Foundation

struct FeaturePlayListResponse: Codable {
    let playlists: PlaylistData
}

struct CategoryPlayListResponse: Codable {
	let playlists: PlaylistData
}


struct PlaylistData: Codable {
    let items: [PlayListResponse] // playlist
}


struct ExternalURLs: Codable {
    let spotify: String
}


struct User: Codable {
    let display_name: String
    let external_urls: ExternalURLs
    let id: String
}


