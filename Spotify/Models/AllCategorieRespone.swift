//
//  AllCategorieRespone.swift
//  Spotify
//
//  Created by rta on 1/14/24.
//

import Foundation

struct AllCategorieRespone: Codable {
	let categories: Categoryies
}

struct Categoryies: Codable {
	let items:[Category]
}

struct Category: Codable {
	let id: String
	let name: String
	let icons: [APIImage]
}
