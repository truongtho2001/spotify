//
//  SearchResultResponse.swift
//  Spotify
//
//  Created by rta on 1/18/24.
//

import Foundation

struct SearchResultResponse: Codable {
	let albums: SearchAlbumResponse
	let artists: SearchArtistsResponse
	let playlists: SearchPlaylistResponse
	let tracks: SearchTracksResponse
}

struct SearchAlbumResponse: Codable {
	let items: [Album]
}

struct SearchArtistsResponse: Codable {
	let items: [Artist]
}

struct SearchPlaylistResponse: Codable {
	let items: [PlayListResponse]
}

struct SearchTracksResponse: Codable {
	let items: [AudioStract]
}
