//
//  RecommendationResponse.swift
//  Spotify
//
//  Created by rta on 12/28/23.
//

import Foundation

struct RecommendationsResponse: Codable {
    let tracks: [AudioStract]
}

