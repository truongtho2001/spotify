//
//  LibraryPlaylistsResponse.swift
//  Spotify
//
//  Created by rta on 2/5/24.
//

import Foundation

struct LibraryPlaylistsResponse: Codable {
	let items: [PlayListResponse]
}
