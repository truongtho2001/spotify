//
//  Playtlist.swift
//  Spotify
//
//  Created by rta on 12/9/23.
//

import Foundation

struct PlayListResponse: Codable {
    let description: String
    let external_urls: ExternalURLs
    let id: String
    let images: [APIImage]
    let name: String
    let owner: User
}
