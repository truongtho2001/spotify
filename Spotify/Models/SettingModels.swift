//
//  SettingModels.swift
//  Spotify
//
//  Created by rta on 12/25/23.
//

import Foundation

struct Section {
    let title: String
    let options:[Option]
}

struct Option {
    let title: String
    let handler: () -> Void
    
}
