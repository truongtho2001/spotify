//
//  APIImage.swift
//  Spotify
//
//  Created by rta on 12/28/23.
//

import Foundation

struct APIImage: Codable {
    let url: String
}
