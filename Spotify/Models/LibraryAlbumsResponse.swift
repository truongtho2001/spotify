//
//  File.swift
//  Spotify
//
//  Created by rta on 3/9/24.
//

import Foundation

struct LibraryAlbumsResponse: Codable {
	let items: [SaveAlbum]
}

struct SaveAlbum: Codable {
	let added_at: String
	let album: Album
}
