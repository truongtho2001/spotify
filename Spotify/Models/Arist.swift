//
//  Arist.swift
//  Spotify
//
//  Created by rta on 12/9/23.
//

import Foundation

struct Artist: Codable {
    let id: String
    let name: String
    let type: String
	let images: [APIImage]?
    let external_urls:[String: String]
    
}
