//
//  SearchResult.swift
//  Spotify
//
//  Created by rta on 1/18/24.
//

import Foundation

enum SearchResult {
	case artist(model: Artist)
	case album(model: Album)
	case track(model: AudioStract)
	case playlist(model: PlayListResponse)
}
