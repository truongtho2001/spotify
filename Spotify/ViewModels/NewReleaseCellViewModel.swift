//
//  NewReleaseCellViewModel.swift
//  Spotify
//
//  Created by rta on 1/1/24.
//

import Foundation

struct NewReleaseCellViewModel {
    let name: String
    let artWorkURL: URL?
    let numberOfTrack: Int
    let artistName: String
}
