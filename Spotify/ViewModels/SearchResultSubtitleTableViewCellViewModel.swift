//
//  SearchResultSubtitleTableViewCellViewModel.swift
//  Spotify
//
//  Created by rta on 1/20/24.
//

import Foundation

struct SearchResultSubtitleTableViewCellViewModel {
	let title: String
	let subtitle: String
	let imageURL: URL?
}
