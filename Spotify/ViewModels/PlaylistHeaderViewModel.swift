//
//  PlaylistHeaderViewModel.swift
//  Spotify
//
//  Created by rta on 1/7/24.
//

import Foundation


struct PlaylistHeaderViewModel {
	let name: String?
	let owerName: String?
	let description: String?
	let artworkURL: URL?
}
