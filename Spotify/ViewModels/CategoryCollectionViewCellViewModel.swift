//
//  CategoryCollectionViewCellViewModel.swift
//  Spotify
//
//  Created by rta on 1/15/24.
//

import Foundation

struct CategoryCollectionViewCellViewModel {
	let title: String
	let artworkURL: URL?
}
