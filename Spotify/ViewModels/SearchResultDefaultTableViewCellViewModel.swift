//
//  SearchResultDefaultTableViewCellViewModel.swift
//  Spotify
//
//  Created by rta on 1/20/24.
//

import Foundation
 
struct SearchResultDefaultTableViewCellViewModel {
	let title: String
	let imageURL: URL?
}
