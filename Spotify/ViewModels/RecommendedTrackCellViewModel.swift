//
//  RecommendedTrackCellViewModel.swift
//  Spotify
//
//  Created by rta on 1/2/24.
//

import Foundation

struct RecommendedTrackCellViewModel {
    let name: String
    let artistName: String
    let artworkURL: URL?
}
