//
//  FeaturedPlaylistCellViewModel.swift
//  Spotify
//
//  Created by rta on 1/2/24.
//

import Foundation

struct FeaturedPlaylistCellViewModel {
    let name: String
    let artworkURL: URL?
    let creatorName: String
}
