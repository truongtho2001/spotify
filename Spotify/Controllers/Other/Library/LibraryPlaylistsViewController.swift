//
//  LibraryPlaylistsViewController.swift
//  Spotify
//
//  Created by rta on 2/4/24.
//

import UIKit

class LibraryPlaylistsViewController: UIViewController {

	var playlists = [PlayListResponse]()
	private let noPlayListiew = ActionLabelView()
	public var selectionHandler:((PlayListResponse) -> Void)?
	
	private let tableView: UITableView = {
		let tableView = UITableView(frame: .zero,style: .grouped)
		tableView.register(SearchResultSubtitleTableViewCell.self, forCellReuseIdentifier: SearchResultSubtitleTableViewCell.identfier)
		tableView.isHidden = true
		return tableView
	}()
	
    override func viewDidLoad() {
        super.viewDidLoad()
		view.backgroundColor = .systemBackground
		tableView.delegate = self
		tableView.dataSource = self
		view.addSubview(tableView)
		setUpNoPlaylistsView()
		fetchData()
		
		if selectionHandler != nil {
			navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .close, target: self, action: #selector(didTapClose))
		}
    }
	
	@objc func didTapClose() {
		dismiss(animated: true, completion: nil)
	}
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		noPlayListiew.frame = CGRect(x: 0, y: 0, width: 150, height: 150)
		noPlayListiew.center = view.center
		tableView.frame = view.bounds
	}
	
	private func setUpNoPlaylistsView() {
		view.addSubview(noPlayListiew)
		noPlayListiew.delegate = self
		noPlayListiew.configture(
			with: ActionLabelViewModel(
				text: "You don't have any playlist yet.",
				acttionTitle: "Create"
			)
		)
	}
	
	
	private func fetchData () {
		DispatchQueue.main.async {
			APICaller.shared.getCurrentUserPlaylists {[weak self] result in
				switch result {
					case .success(let playlists):
						self?.playlists = playlists
						DispatchQueue.main.async {
							self?.updateUI()
						}
						break
					case .failure(let error):
						print(error.localizedDescription)
				}
			}
		}
	}
	private func updateUI() {
		if playlists.isEmpty {
			//Show label
			noPlayListiew.isHidden = false
			tableView.isHidden = true
		}
		else {
			//Show table
			tableView.reloadData()
			noPlayListiew.isHidden = true
			tableView.isHidden = false
		}
	}
    
	public func showCreatePlayListAlert() {
		let alert = UIAlertController(
			title: "New Playlists ",
			message: "Enter playlist name.",
			preferredStyle: .alert
		)
		alert.addTextField{ textField in
			textField.placeholder = "Playlist..."
			alert.addAction(UIAlertAction(title: "Cancel", style: .cancel,handler: nil))
			alert.addAction(UIAlertAction(title: "Create", style: .default,handler: { _ in
				guard let field = alert.textFields?.first,
					  let text = field.text,
					  !text.trimmingCharacters(in: .whitespaces).isEmpty else {
					return
				}
				
				APICaller.shared.createPlaylist(with: text) { success in
					if success {
						HapticsManager.shared.vibrate(for: .success )
						//Refresh list of playlists
						self.fetchData()
					}
					else {
						HapticsManager.shared.vibrate(for: .error  )
						print("Failed to create playlist")
					}
				}
			}))
		}
		present(alert, animated: true, completion: nil) 
	}
}


extension LibraryPlaylistsViewController: ActionLabelViewDelegate {
	func actionLabelViewDidTapButton(_ actionView: ActionLabelView) {
		//Show Creation UI
		showCreatePlayListAlert()
	}
}

extension LibraryPlaylistsViewController:UITableViewDelegate, UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return playlists.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(
			withIdentifier: SearchResultSubtitleTableViewCell.identfier,
			for: indexPath
		) as? SearchResultSubtitleTableViewCell else {
			return UITableViewCell()
		}
		let playlist = playlists[indexPath.row]
		cell.configure(with: SearchResultSubtitleTableViewCellViewModel(title: playlist.name , subtitle: playlist.owner.display_name, imageURL:URL(string:  playlist.images.first?.url ?? "")))
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		HapticsManager.shared.vibrateForSelection()
		let playlist = playlists[indexPath.row]
		guard selectionHandler == nil else {
			selectionHandler?(playlist)
			dismiss(animated: true,completion: nil)
			return
		}
		let vc = PlaylistViewController(playlist: playlist)
		vc.navigationItem.largeTitleDisplayMode = .never
		navigationController?.pushViewController(vc, animated: true )
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 70
	}
}

