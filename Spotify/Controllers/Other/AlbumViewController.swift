	//
	//  AlbumViewController.swift
	//  Spotify
	//
	//  Created by rta on 1/3/24.
	//

	import UIKit

	class   AlbumViewController: UIViewController {
		
		
		private let collectionView = UICollectionView(
			frame: .zero,
			collectionViewLayout: UICollectionViewCompositionalLayout(sectionProvider: {  _, _ -> NSCollectionLayoutSection? in
				//Item
				let item = NSCollectionLayoutItem(
					layoutSize: NSCollectionLayoutSize(
						widthDimension: .fractionalWidth(1.0),
						heightDimension: .fractionalHeight(1.0)
					)
				)
				
				item.contentInsets = NSDirectionalEdgeInsets(top: 1, leading: 2, bottom: 1, trailing: 2)
				//Group
				let horizontalGroup = NSCollectionLayoutGroup.vertical(
					layoutSize: NSCollectionLayoutSize(
						widthDimension: .fractionalWidth(1),
						heightDimension: .absolute(60)),
					subitem: item ,
					count: 1)
				//Section
				let section = NSCollectionLayoutSection(group: horizontalGroup)
				section.boundarySupplementaryItems = [
					NSCollectionLayoutBoundarySupplementaryItem(
						layoutSize: NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
														   heightDimension:.fractionalWidth(1)),
						elementKind: UICollectionView.elementKindSectionHeader,
						alignment: .top)]
				return section
			})
		)
		
		private var viewModels = [RecommendedTrackCellViewModel]()

		private var tracks = [AudioStract]()
		private let album: Album
		
		init(album: Album) {
			self.album = album
			super.init(nibName: nil, bundle: nil)
		}
		
		required init?(coder: NSCoder) {
			fatalError()
		}

		
		override func viewDidLoad() {
			super.viewDidLoad()
			title = album.name
			view.backgroundColor = .systemBackground
			
			view.addSubview(collectionView)
			collectionView.register(
				AlbumTrackCollectionViewCell.self,
				forCellWithReuseIdentifier: AlbumTrackCollectionViewCell.indentifier)
			collectionView.register(
				PlaylistHeaderCollectionReusableView.self,
				forSupplementaryViewOfKind:UICollectionView.elementKindSectionHeader,
				withReuseIdentifier: PlaylistHeaderCollectionReusableView.identifier )
			collectionView.backgroundColor = .systemBackground
			collectionView.delegate = self
			collectionView.dataSource = self
			fetchData()
			navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(didTapActions))
		}
		
		@objc func didTapActions() {
			let actionSheet = UIAlertController(title: album.name, message: "Actions", preferredStyle: .actionSheet)
			actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
			actionSheet.addAction(UIAlertAction(title: "Save album", style:.default, handler: { [weak self] _ in
				guard let strongSelf = self else { return }
				APICaller.shared.saveAlbums(album: strongSelf.album) { success in
 					if success {
						HapticsManager.shared.vibrate(for: .success )
						NotificationCenter.default.post(name: .albumSavedNotification, object: nil)
					} else {
						HapticsManager.shared.vibrate(for: .error )
					}
				}
			}))
			present(actionSheet, animated: true)
		}
		
		public func fetchData() {
			APICaller.shared.getAlbumDetails(for: album) { [weak self] reslut in
				DispatchQueue.main.async {
					switch reslut {
						case .success(let model):
							self?.tracks = model.tracks.items
							self?.viewModels = model.tracks.items.compactMap({
								RecommendedTrackCellViewModel(
									name: $0.name,
									artistName: $0.artists.first?.name ?? "-",
									artworkURL: URL(string: $0.album?.images.first?.url ?? "")
								)
							})
							self?.collectionView.reloadData()
						case .failure(let error):
							print(error.localizedDescription)
					}
				}
			}
		}
		
		override func viewDidLayoutSubviews() {
			super.viewDidLayoutSubviews()
			collectionView.frame = view.bounds
		}
	}




	extension AlbumViewController: UICollectionViewDelegate, UICollectionViewDataSource {
		
		func numberOfSections(in collectionView: UICollectionView) -> Int {
			return 1
		}
		
		func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
			return viewModels.count
		}
		
		func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
			guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AlbumTrackCollectionViewCell.indentifier, for: indexPath)
					as? AlbumTrackCollectionViewCell else {
				return UICollectionViewCell()
			}
			cell.backgroundColor = .red
			cell.configure(with: viewModels[indexPath.row])
			return cell
		}
		
		func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
			guard let header = collectionView.dequeueReusableSupplementaryView(
				ofKind: kind,
				withReuseIdentifier:PlaylistHeaderCollectionReusableView.identifier,
				for:indexPath
			) as? PlaylistHeaderCollectionReusableView,
				  kind == UICollectionView.elementKindSectionHeader else {
				return UICollectionReusableView()
			}
			let headerViewModel = PlaylistHeaderViewModel(
				name: album.name,
				owerName: album.artists.first?.name,
				description: "Release Date: \(String.formattedDate(string: album.release_date))",
				artworkURL: URL(string: album.images.first?.url ?? ""))
			header.configure(with: headerViewModel)
			header.delegate = self
			return header
		}
		
		func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
			collectionView.deselectItem(at: indexPath, animated: true)
			//Play Song
			var track = tracks[indexPath.row]
			track.album = self.album
			PlaybackPresenter.shared.startPlayback(from: self, track: track)
		}
	}

	extension AlbumViewController: PlaylistHeaderCollectionReusableViewDelegate {
		func playlistHeaderCollectionReusableViewDidTapPlayAll(_ header: PlaylistHeaderCollectionReusableView) {
			// start play list play in queue
			let tracsWithAlbum: [AudioStract] = tracks.compactMap({
				var track = $0
				track.album = self.album
				return track
			})
			PlaybackPresenter.shared.startPlayback(from: self, tracks : tracsWithAlbum)
		}
	}
